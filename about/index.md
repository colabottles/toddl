---
layout: layouts/about.njk
title: About Me
date: 2019-06-05
tags:
  - nav
navtitle: About
templateClass: tmpl-post
---

Hi, I'm Todd. I am a Senior Frontend Developer and Lead Technical Support Specialist at Agency Revenue Tools in Conway, New Hampshire.

I don't know who you are. I don't know what you want. If you are looking for a frontend developer I can tell you I don't have every skill, but what I do have are a very particular set of skills. Skills I have acquired over a very long career. Skills that make me a benefit for people like you. If you let my resume go now that'll be the end of it. I will not look for you, I will not pursue you, but if you don't, I will look for you, I will find you and I will work for you.

* HTML
* CSS
  * Sass
  * Grid Layout
  * Flex/Flexbox
* JavaScript
  * Vanilla
  * JQuery
  * Node
* Static Site Genrators
  * Eleventy
  * Jekyll
  * Hugo
  * Middleman
* Responsive Web Design
  * Mobile First
* WCAG A11y
  * 2.0 AA &amp; AAA
* CMS
  * WordPress (since it was b2/Cafelog)
  * Drupal
* Adobe CS
  * Illustrator
  * Photoshop
  * XD
* Sketch
* Figma
* Git
* CLI
* Vim

Here's how this site works;

* [Netlify](https://netlify.com) 
* [GitHub](https://github.com) 
* [Code](https://code.visualstudio.com/)
* [iTerm2](https://iterm2.com/) 
* [Eleventy](https://11ty.io)

I have a passion for the web and making things on the web. Taking things apart and building them back together, if not better. I am interested in a lot of different things, mainly outdoors and travel. Photography and typography. Design and User Experience. Food and people.

My goals in life are to find a home in a agency or firm and finish out my days in web development there. I have worked with many different people and places on projects, and the list of clients whom I have done work for in some capacity is long but I can send a list if anyone is itnerested. They are not current or even recent projects though.a11y

I am self-taught 100%. I started learning BASIC and now I am learning all sorts of new technologies like Ember and Vue. Even React for the sole reason as to use Gatsby. 

My education is a weird story, I went to school at an accelerated program in Southern California, but after I left and moved away, the school went bankrupt and abruptly closed its doors. So I guess that Associates Degree in Visual Communication really does not mean much, but it's there.

Thanks for reading and <a href="{{ '/' | url }}/contact">use the contact form to get in touch</a> so we can talk about me filling a role at your company. I am also willing (very willing if it a warm climate) to relocate.
