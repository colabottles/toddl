---
title: "That Guy"
description: A story about myself, being helpful, and doing better than yesterday.
date: 2019-08-01
tags:
  - about
  - bio
layout: layouts/post.njk
---

I was <span class="italic">&ldquo;That dude&rdquo;</span>. Or <span class="italic">&ldquo;That guy&rdquo;</span>. The guy that thought his opinion was the one that mattered. It doesn’t matter I learned. I learned through a series of stupid tweets, Medium articles, and blog editorials. Shit posts and hot takes.

I was angry. Very angry. With a hatred for all things not me. People, places, things. If you had no purpose, if something didn’t serve me, if I was somewhere I did not want to be. I hated it. A seething, white-hot hatred.

Then I got sober in the late summer of 2013. That hatred grew exponentially. I was raw. I was newly sober and I could not medicate myself. I was even angrier.

I was the guy that trolled. I was the guy that used <span class="italic">&ldquo;sarcasm&rdquo;</span>. I was the guy that had to be right. I wasn’t. I’m not, I never will be. I had a lot to learn. I still do. I still even fall back into old habits. I’m constantly learning.

I saw someone on Twitter tweeting about going through a hard time and it brought me to where I am, where I have been, and the work I need to do to not <span class="italic">&ldquo;be an asshole&rdquo;</span>. Believe me, I am trying my hardest to right the ship.

That’s what I was. That’s what I am from time-to-time. An asshole. I remember telling a person on Twitter to <span class="bolder">&ldquo;RTFM&rdquo;</span> when they tweeted about not understanding CSS. <span class="bolder">&ldquo;RTFM&rdquo;</span> something I always loathed and abhorred seeing, but yet here I was. The hypocrite.

I don’t have many friends in this thing called <span class="italic">&ldquo;the real world&rdquo;</span>. I just don’t. Not since I stopped drinking. They all went away, as most do when one stops drinking to excess and partying non-stop. I kind of thrive with interaction online. Most of it on Twitter which I cannot stand to be on but use anyway (still a hypocrite).

I have to be vigilant with a couple things. One being how I interact. I haven’t been doing a great job lately. My depression has kicked in since some family issues arose starting last year, yet those are not excuses for my behavior. I have no excuses. I have learned to take responsibility for my actions. I call myself out.

I just started thinking about a lot tonight. I was that guy that didn’t care. Even as far as being called the <span class="italic">&ldquo;Gordon Ramsay of web developers&rdquo;</span> way back when all we knew of Ramsay was screaming at people and cursing. And I did that on this message board I was a moderator of. That's embarrassing.

99.95% of the time now I can see something and just walk away, scroll by, turn the other cheek, not a word. Total silence. Those times though, where I inexplicably get pulled in. Those are the worst. Those little moments, those lapses in judgment, those cost me big. I’m <span class="italic">&ldquo;that guy&rdquo;</span>… again.

Chris Coyier reminded me last night in an article that <span class="italic">&ldquo;Being too opinionated about right/wrong in tooling choices isn’t helpful.&rdquo;</span> Well that goes the same regarding interaction in life. Being too opinionated isn’t helpful. Sarcasm isn’t needed everywhere. I’m not Don Rickles. This isn’t a Comedy Central Roast.

I was <span class="italic">&ldquo;That dude&rdquo;</span>. That is not helpful. I have to be better than I was yesterday. I used to say that to myself every day, then I stopped. Then I stopped being better than yesterday and it shows. So, I wake up and right next to me is my piece of paper sitting right there which reads, <span class="italic">&ldquo;Be better than you were yesterday&rdquo;</span>.

<img src="/img/notetoself.png" alt="A piece of paper on my side table that reads, 'Be better than you were yesterday'." />

It’s the first thing I look at when my eyes open. Be helpful, not an asshole.
