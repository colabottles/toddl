---
title: An Event Apart 2019 - Denver Wrap-Up Edition, Part Three
description: The wrap-up article after An Event Apart in Denver, Colorado.
date: 2019-10-31
tags:
  - conferences
layout: layouts/post.njk
---

## An Event Apart Denver 2019, Denver, CO

### Day Three - The Final Chapter

I never look forward to the final day because it’s the final day. With all good things however, they must come to an end. Kate O'Neill kicked off Day Three with human experiences in technology. Kate went into business objectives and what humans are trying to accomplish through those experiences. Building our best tech, grow our best businesses, and become our best selves. Kate touched on meaning and innovation as she spoke about things like Augmented Reality (AR), machine work, and technological capabilities. I was in Boston when Kate gave this talk and it was a good reminder to hear it again. Especially when she touched upon the Amazon Go store. People can't help other people get things off the shelf at an Amazon Go store, which takes the humanity out of the shopping experience creating a poor experience.

Sarah Drasner took the stage after with "Animation on the Bleeding Edge". Sarah spoke about emotions and how "the web is first and foremost about communicating". She then showed everyone Nuxt and it is impeccable timing for me to be here as I am going to be (and I already am when I can) learning Vue.js. Showing animations in Vue 2 using GreenSock. This looks like something I could really enjoy learning for sure. Different types of animations and their responsive qualities, but then Sarah took us through responsiveness in 3D! This part of her talk was terrific. Some terrific, mind-blowing demos from Sarah making me want to learn Vue and Nuxt now. This was the first time I have seen and heard Sarah speak and it was truly as phenomenal as her slides of her artwork were.

Val Head came up with "Making Motion Inclusive" and started off with motion. Referring to the WCAG, Val took us through the guidelines. Flashing items, movement and animation within content that may be harmful (seizure inducing triggers) on the WCAG 2.0 AA & AAA levels. Val went into animation that affects folks with vestibular orders such as motion sickness and migraines. How those animations and things like parallax can physically make someone with a vestibular order (like myself) sick. Val then went into talking about `refers-reduced-motion`. Another talk I saw in Boston (that you can also read about in the archives) and another talk I was glad to hear again for sure.

So for lunch I decided to go out for lunch and meet up with my friend Chris DeMars. Chris, Jason Pamental, and I went to a local Mexican restaurant, La Loma, was phenomenal. Not much time to spent, but time well spent indeed and good food, good conversation.

Back at the conference, Derek Featherstone kicked off the last half of the final day with "Inclusive, by Design". We need to think in more diverse ways with regards to accessibility and to think of these things by default. Derek walked us through case studies and inclusion on the web and with web apps. If we include people with disabilities in the process, things will be much better. "Acknowledge your privilege, and your power, and your position in the design process." says Derek. "Ask 'How can we make sure that we include more people with disabilities earlier and more meaningfully in the process?" Derek had a lot of questions that we need to ask ourselves to be more inclusive in designs and the design process, and to include people with disabilities in that process. This is another talk I have heard, again in Boston. Well worth hearing and seeing it again.

and as with the last few AEA events I have attended, Gerry McGovern ended the conference with his stellar (and very humorous) talk, "Delivering Product 67". I just finished Gerry's book "Top Tasks" and it was brilliant. I have applied things in that book to the company I work for to better our product. Humility, Agility, and Simplicity. Gerry shows us how to enhance those qualities to support our company's business and advance our careers. Master the metrics of customer simplicity. I had the pleasure and privilege to have a couple of great conversations over breakfast with Gerry and he is a delight to speak with. All the speakers are! (More on that later). How to keep the garbage off of our pages, stopping the customer from doing what they really need to do. There is too much garbage on the web. Gerry points this out with the many iterations of the Google homepage.

and with that, the conference was over.

### The After-Conference

I consider myself fortunate, lucky, and pretty damn grateful. With that said, I got to hang out with Jason Pamental and Miriam Suzanne and a new conference friend for some time, eating Indian cuisine at a local place Miriam took us too nearby, then hanging out at the restaurant at the hotel and talking for hours. During the way, I had met TJ Pitre and we got to talking as well. This is why I enjoy the conferences I go to. I meet a mix of people far and wide. I don't want to meet the same people over and over and over who are like me. Which is why I strongly advocate for diversity and inclusion. Which is why Farai's talk hit me at the very core.

After some lengthy conversation I retired to my room and then prepared to rest for the long trek back home. I really don't like the end process after the conferences because I meet so many great people and talk to some very smart folks in the course of three days (even more because I get there a day early and stay a day late).

If you are a designer or developer, or in web of any kind, you MUST go to An Event Apart. Whether it is in your city or whether you have to travel. It is well worth it. A conference that is diverse and inclusive, no egos, and all the learning and camraderie you can find all in one place. To get your boss(es) to pay for you and/or your team to go, <a href="https://aneventapart.com/why-attend">go here and read about all the things said</a> and don't forget that towards the end of the page are links for some great pointers to persuade your boss to send you!
